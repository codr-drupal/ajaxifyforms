Ajaxify submit forms
--------------------

Maintainers:
 Marouan Hammami mh.marouan@gmail.com / https://www.drupal.org/user/3203501

SUMMARY:

Ajaxify submit forms module will AJAXify any Drupal form. This essentially 
allows for validation, preview, and submission without a page refresh.

Uses Drupal Ajax responses (https://api.drupal.org/api/drupal/
core%21core.api.php/group/ajax/8.2.x).
Shows validation error messages above the form.

How it works
-After enable the module, visit Configuration -> Development -> 
ajaxify submit form.
-Add list of ID form, Example : Form ID ="user_register_form" and 
Action ="submit".
